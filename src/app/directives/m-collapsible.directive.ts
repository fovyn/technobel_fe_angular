import { Directive, ElementRef } from '@angular/core';
import * as M from 'materialize-css';

@Directive({
  selector: '[mCollapsible]'
})
export class MCollapsibleDirective {

  constructor(private elRef: ElementRef<HTMLUListElement>) { 
    M.Collapsible.init(this.elRef.nativeElement);
  }

}
